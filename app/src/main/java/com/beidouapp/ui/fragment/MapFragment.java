package com.beidouapp.ui.fragment;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.beidouapp.R;
import com.beidouapp.model.utils.TianDiTu;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.layers.WebTiledLayer;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbolLayer;

public class MapFragment extends Fragment {


    private MapView TdtmapView;
    private LocationDisplay mLocationDisplay;
    private GraphicsOverlay mGraphicsLayer;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ini(view);
        ini_Listener();

        return view;
    }

    private void ini(View view) {
        TdtmapView = view.findViewById(R.id.TdtMap);
        TdtmapView.setAttributionTextVisible(false);
        WebTiledLayer webTiledLayer2 = TianDiTu.CreateTianDiTuTiledLayer(TianDiTu.LayerType.TIANDITU_IMAGE_2000);
        Basemap tdtBasemap2 = new Basemap(webTiledLayer2);
        WebTiledLayer webTiledLayer22 = TianDiTu.CreateTianDiTuTiledLayer(TianDiTu.LayerType.TIANDITU_IMAGE_ANNOTATION_CHINESE_2000);
        tdtBasemap2.getBaseLayers().add(webTiledLayer22);

        ArcGISMap map = new ArcGISMap(tdtBasemap2);
        TdtmapView.setMap(map);
        TdtmapView.setViewpoint(new Viewpoint(34.77669, 113.67922, 10000));

        mLocationDisplay = TdtmapView.getLocationDisplay();
        mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.NAVIGATION);

        if (!mLocationDisplay.isStarted())
            mLocationDisplay.startAsync();
        mLocationDisplay.addLocationChangedListener(new LocationDisplay.LocationChangedListener() {
            @Override
            public void onLocationChanged(LocationDisplay.LocationChangedEvent locationChangedEvent) {
                double longitude = locationChangedEvent.getLocation().getPosition().getX();
                double latitude = locationChangedEvent.getLocation().getPosition().getY();
                Log.d("zw_TDT", "onLocationChanged: 使用Arcgis获取的自身的位置是" + longitude + "  " + latitude);
            }
        });

    }

    private void ini_Listener() {
        TdtmapView.setOnTouchListener(new DefaultMapViewOnTouchListener(getActivity().getApplicationContext(), TdtmapView)
        {
            @Override
            public void onLongPress(MotionEvent e) {
                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(e.getX()), Math.round(e.getY()));
                Point clickPoint = mMapView.screenToLocation(screenPoint);// 屏幕坐标转地理坐标
                GraphicsOverlay graphicLayer = getGraphicLayer();
                if(graphicLayer != null){
                    BitmapDrawable image = (BitmapDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.black);
                    PictureMarkerSymbol pictureMarkerSymbol = new PictureMarkerSymbol(image);
                    pictureMarkerSymbol.setHeight(40);
                    pictureMarkerSymbol.setWidth(40);
                    //Optionally set the offset, to align the base of the symbol aligns with the point geometry
                    pictureMarkerSymbol.setOffsetY(11); //The image used for the symbol has a transparent buffer around it, so the offset is not simply height/2
                    pictureMarkerSymbol.loadAsync();
                }
                super.onLongPress(e);
            }
        });
    }

    private GraphicsOverlay getGraphicLayer() {
        if(mGraphicsLayer == null){
            mGraphicsLayer = new GraphicsOverlay();
            TdtmapView.getGraphicsOverlays().add(mGraphicsLayer);
        }
        return mGraphicsLayer;
    }

    @Override
    public void onResume() {
        super.onResume();
        TdtmapView.resume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TdtmapView.dispose();
    }

    @Override
    public void onPause() {
        super.onPause();
        TdtmapView.pause();
    }
}